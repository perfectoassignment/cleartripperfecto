package com.cleartrip.bean;

public class HotelBean {
	
	String hotelName;
	String hotelPrice;
	//String HotelTotalPrice;
	String hotelRating;
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelPrice() {
		return hotelPrice;
	}
	public void setHotelPrice(String hotelPrice) {
		this.hotelPrice = hotelPrice;
	}
	/*public String getHotelTotalPrice() {
		return HotelTotalPrice;
	}
	public void setHotelTotalPrice(String hotelTotalPrice) {
		HotelTotalPrice = hotelTotalPrice;
	}*/
	public String getHotelRating() {
		return hotelRating;
	}
	public void setHotelRating(String hotelRating) {
		this.hotelRating = hotelRating;
	}
	
	

}
