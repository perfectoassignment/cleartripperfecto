package com.cleartrip.component;


import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ComponentCleartripPage extends QAFWebComponent {

	
	

	public ComponentCleartripPage(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}



	@FindBy(locator = "trainnumber.searchresultspage.xpath")
	private QAFWebElement trainnumberSearchresultspageXpath;

	
	

	public QAFWebElement getTrainnumberSearchresultspageXpath() {
		return trainnumberSearchresultspageXpath;
	}

}
