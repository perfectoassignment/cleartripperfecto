package com.cleartrip.component;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HotelResultPageComponent extends QAFWebComponent {

	public HotelResultPageComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}
	
	public QAFWebElement getHotelName() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "txtHotelName"));
		return element;
	}
	
	public QAFWebElement getHotelPrice() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "txtHotelPrice"));
		return element;
	}
	
	public QAFWebElement getHotelTotalPrice() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "tvTotalPrice"));
		return element;
	}
	
	public QAFWebElement getHotelRating() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "txtTripAdvisorRatings"));
		return element;
	}

}
