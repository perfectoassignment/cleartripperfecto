package com.cleartrip.pages;

import java.util.List;

import org.testng.Reporter;

import com.cleartrip.bean.HotelBean;
import com.cleartrip.component.HotelResultPageComponent;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;


public class HotelSearchResultsTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "resource-id.common.hotelSearchResultsPage")
	private QAFWebElement resource_idCommonHotelSearchResultsPage;
	@FindBy(locator = "list.hotels.hotelSearchResultsPage")
	private QAFWebElement listHotelsHotelSearchResultsPage;
	@FindBy(locator = "btn.ok.gotIt.hotelSearchResultsPage")
	private QAFWebElement btnUserSuggestionOkayGotIt;
	@FindBy(locator = "btn.fliter.hotelSearchResultsPage")
	private QAFWebElement btnFilter;
	@FindBy(locator = "chk.fliter.option.payAtHotel.hotelSearchResultsPage")
	private QAFWebElement chkPayAtHotel;
	@FindBy(locator = "chk.fliter.option.starRating.hotelSearchResultsPage")
	private QAFWebElement chkStarRating;
	@FindBy(locator = "btn.filter.apply.hotelSearchResultsPage")
	private QAFWebElement btnFilterApply;
	@FindBy(locator = "btn.sort.hotelSearchResultsPage")
	private QAFWebElement btnsort;
	@FindBy(locator = "btn.sort.option.hotelSearchResultsPage")
	private QAFWebElement lnkSortLowToHigh;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getResource_idCommonHotelSearchResultsPage() {
		return resource_idCommonHotelSearchResultsPage;
	}

	public QAFWebElement getListHotelsHotelSearchResultsPage() {
		return listHotelsHotelSearchResultsPage;
	}

	@FindBy(locator = "list.hotels.hotelSearchResultsPage")
	private List<HotelResultPageComponent> hotelSearchResults;

	public List<HotelResultPageComponent> getHotelSearchResults() {
		return hotelSearchResults;
	}
	
	public QAFWebElement getOkayGotItLnk() {
		return btnUserSuggestionOkayGotIt;
	}
	
	public QAFWebElement getFilterLayOut() {
		return btnFilter;
	}
	
	public QAFWebElement getPayAtHotel() {
		return chkPayAtHotel;
	}
	
	public QAFWebElement getHotelStarRating() {
		return chkStarRating;
	}
	
	public QAFWebElement getFilterApplyBtn() {
		return btnFilterApply;
	}
	
	public QAFWebElement getHoteSearchSort() {
		return btnsort;
	}
	
	public QAFWebElement getFilterLowToHigh() {
		return lnkSortLowToHigh;
	}
	
	/*public QAFWebElement getFilterLayOut() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "filterLayout"));
		return element;
	}
	
	public QAFWebElement getPayAtHotel() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "checkBoxPayAtHotelApplied"));
		return element;
	}
	
	public QAFWebElement getHotelStarRating() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "starRating5"));
		return element;
	}
	
	public QAFWebElement getFilterApplyBtn() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "hotelfliterApplyBtn"));
		return element;
	}
	
	public QAFWebElement getHoteSearchSort() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "sortLayout"));
		return element;
	}
	
	public QAFWebElement getFilterLowToHigh() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "priceLowTxt"));
		return element;
	}
	
	public QAFWebElement getOkayGotItLnk() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("resource-id.common.hotelSearchResultsPage"), "hotel_360_okay_button"));
		return element;
	}*/
	
	public void filters() {
		getFilterLayOut().click();
		//getPayAtHotel().isDisplayed();
		//getPayAtHotel().click();
		getHotelStarRating().click();
		getFilterApplyBtn().click();
		
		getHoteSearchSort().isDisplayed();
		getHoteSearchSort().click();
		getFilterLowToHigh().isDisplayed();
		getFilterLowToHigh().click();
	}
	public void applyFilters() {
		try {
			filters();
		} catch (Exception e) {
			getOkayGotItLnk().click();
			filters();
		}
		
		
	}
	
	public void selectHotelByIndex(int index) {
		Reporter.log("selecting hotel based on index");
		HotelBean bean = new HotelBean();
		bean.setHotelName(getHotelSearchResults().get(index).getHotelName().getAttribute("text").trim());
		bean.setHotelPrice(getHotelSearchResults().get(index).getHotelPrice().getAttribute("text"));
		//bean.setHotelTotalPrice(getHotelSearchResults().get(index).getHotelTotalPrice().getAttribute("text"));
		bean.setHotelRating(getHotelSearchResults().get(index).getHotelRating().getAttribute("text"));
		ConfigurationManager.getBundle().setProperty("hotel.bean", bean);

		getHotelSearchResults().get(index).click();
		
	}
	
	public  void isOkayLnkPresentClick() {
		 getOkayGotItLnk().isDisplayed();
		 getOkayGotItLnk().click();
	}
}
