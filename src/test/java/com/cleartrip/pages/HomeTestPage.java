package com.cleartrip.pages;
import java.util.HashMap;
import java.util.Map;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.allowDeviceLocation.homepage")
	private QAFWebElement idAllowDeviceLocationHomepage;
	@FindBy(locator = "btn.pageSwitcher.homepage")
	private QAFWebElement idPageSwitcherHomepage;
	@FindBy(locator = "xpath.menuSelect.dynamic.homepage")
	private QAFWebElement xpathMenuSelectDynamicHomepage;
	@FindBy(locator = "lnk.hotels.homepage")
	private QAFWebElement hotelsPage;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getIdAllowDeviceLocationHomepage() {
		return idAllowDeviceLocationHomepage;
	}

	public QAFWebElement getPageSwitcher() {
		return idPageSwitcherHomepage;
	}
	
	public QAFWebElement getXpathMenuSelectDynamicHomepage() {
		return xpathMenuSelectDynamicHomepage;
	}

	public QAFWebElement getHotelsTab() {
		return hotelsPage;
	}
	
	public QAFWebElement getAcvtivitiesTab() {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("xpath.menuSelect.dynamic.homepage"), "Activities"));
		return element;
	}

	/*public QAFWebElement getHotelsTab() {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("xpath.menuSelect.dynamic.homepage"), "Hotels"));
		return element;
	}*/

	public boolean isPresent(QAFWebElement element) {
		return element.isPresent();
	}
	
	public void installApp() {
		Map<String, Object> params = new HashMap<>();
		  
		params.put("file", "PUBLIC:cleartrip1/com.cleartrip.android_2018-04-18.apk");
		params.put("instrument", "noinstrument");
		driver.executeScript("mobile:application:install", params); 
	}

	public boolean allowDeiceLocation() {
		return getIdAllowDeviceLocationHomepage().isDisplayed();
	
	}
	
	public void selectPageSwitcher() {
		getPageSwitcher().isDisplayed();
		Reporter.log("clicking on element switcher");
		getPageSwitcher().click();
	}

	
	public void switchAndSelectHotelsTab() {
		Reporter.log("checking element present Activities: "+getPageSwitcher().isDisplayed());
		getPageSwitcher().isDisplayed();
		Reporter.log("clicking on element switcher");
		getPageSwitcher().click();
		Reporter.log("checking element present Activities: "+getHotelsTab().isDisplayed());
		getHotelsTab().isDisplayed();
		Reporter.log("clicking on element Hotels");
		getHotelsTab().click();
	}
	public void selectHotelsTab() {	
		
		try {
			Reporter.log("checking element present Activities: "+getHotelsTab().isDisplayed());
			getHotelsTab().isDisplayed();
			Reporter.log("clicking on element Hotels");
			getHotelsTab().click();
		} catch (Exception e) {
			if(!allowDeiceLocation()) 
			{
				getIdAllowDeviceLocationHomepage().click();
				switchAndSelectHotelsTab();
			}else
			{
				switchAndSelectHotelsTab();
			}
		}
		
		
	}
}
