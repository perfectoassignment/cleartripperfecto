package com.cleartrip.steps;

import com.cleartrip.pages.HotelDetailsTestPage;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class TrainTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "train.subOption")
	private QAFWebElement trainSubOption;
	@FindBy(locator = "txt.liveTrainStatus")
	private QAFWebElement txtliveTrainStatus;
	@FindBy(locator = "txt.trainStatusTextBox")
	private QAFWebElement txtTrainStatus;
	@FindBy(locator = "txt.trainName")
	private QAFWebElement txtTrainName;
	@FindBy(locator = "train.Status")
	private QAFWebElement trainSatus;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getTrainSubOption() {
		return trainSubOption;
	}

	public QAFWebElement getLiveTrainStatus() {
		return txtliveTrainStatus;
	}
	@QAFTestStep(description = "user open the application on device")
	public void launchApp() {
		Reporter.log("Application is launch.");

	}
	@QAFTestStep(description = "user open {0} menu")
	public void userOpenMenu(String menu) {
		QAFWebElement trainMenu =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("xpath.menuSelect.dynamic.homepage"), menu));
		trainMenu.isDisplayed();
		trainMenu.click();
	}
	@QAFTestStep(description = "user click on {0} link")
	public void userClickLiveStatusLink(String str0) {
		QAFWebElement trainStatusMenu =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("xpath.menuSelect.dynamic.homepage"), str0));
		trainStatusMenu.isDisplayed();
		trainStatusMenu.waitForVisible();
		trainStatusMenu.click();
	}

	@QAFTestStep(description = "user click on text box and send name of train")
	public void userClickOnTextBox() {
		txtliveTrainStatus.isDisplayed();
		txtliveTrainStatus.click();
		txtliveTrainStatus.sendKeys("12124");
		txtTrainName.isDisplayed();
		txtTrainName.click();
	}

	@QAFTestStep(description = "user see the status of Train")
	public void userSeeTheStatusOfTrain() {
		trainSatus.isDisplayed();
		Reporter.log("Status of Train:" + trainSatus.getText());
	}

	@QAFTestStep(description = "user close the application")
	public void userCloseTheApplication() {
		HotelDetailsTestPage detailsTestPage=new HotelDetailsTestPage();
		detailsTestPage.closeApp();
	}

}
