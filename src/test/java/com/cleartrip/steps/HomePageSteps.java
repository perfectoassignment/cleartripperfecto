package com.cleartrip.steps;

import com.cleartrip.pages.HomeTestPage;
import com.cleartrip.pages.HotelDetailsTestPage;
import com.cleartrip.pages.HotelsSearchTestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;

public class HomePageSteps {


	HomeTestPage homePge = new HomeTestPage();
	HotelsSearchTestPage hotelSearchPage = new HotelsSearchTestPage();
	HotelDetailsTestPage hotelDetailPage = new HotelDetailsTestPage();

	@QAFTestStep(description = "user open cleartrip application")
	public void userOpenCleartripApplication() {
		//homePge.installApp();
		/*try {
			hotelDetailPage.closeApp();
		} catch (Exception e) {
			Reporter.log("application not in opened state");
		}*/
		System.out.println("opening application");
	}

	@QAFTestStep(description = "user allow any authentication/advertisement if it comes")
	public void userAllowAnyAuthenticationAdvertisementIfItComes() {
		homePge.allowDeiceLocation();
	}

	@QAFTestStep(description = "user selects hotels tab from menu")
	public void userSelectsHotelsTabFromMenuButtens() {
		//homePge.selectPageSwitcher();
		homePge.selectHotelsTab();
	}
}
