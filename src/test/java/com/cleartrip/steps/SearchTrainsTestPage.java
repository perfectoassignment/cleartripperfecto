package com.cleartrip.steps;

// import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SearchTrainsTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "searchpage.tocitybutton.id")
	private QAFWebElement searchpageTocitybuttonId;

	@FindBy(locator = "searchpage.fromcitybutton.id")
	private QAFWebElement searchpageFromcitybuttonId;

	@FindBy(locator = "searchpage.citybutton.xpath")
	private QAFWebElement searchpageCitybuttonXpath;

	@FindBy(locator = "searchpage.departuredate.xpath")
	private QAFWebElement searchpageDeparturedateXpath;

	@FindBy(locator = "searchpage.classbutton.xpath")
	private QAFWebElement searchpageClassbuttonXpath;

	@FindBy(locator = "searchpage.searchtrainsbutton.id")
	private QAFWebElement searchpageSearchtrainsbuttonId;

	@FindBy(locator = "departuredate.searchpage.id")
	private QAFWebElement departuredateSearchpageId;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getSearchpageTocitybuttonId() {
		return searchpageTocitybuttonId;
	}

	public QAFWebElement getSearchpageFromcitybuttonId() {
		return searchpageFromcitybuttonId;
	}

	public QAFWebElement getSearchpageCitybuttonXpath() {
		return searchpageCitybuttonXpath;
	}

	public QAFWebElement getSearchpageDeparturedateId() {
		return searchpageDeparturedateXpath;
	}

	public QAFWebElement getSearchpageClassbuttonXpath() {
		return searchpageClassbuttonXpath;
	}

	public QAFWebElement getSearchpageSearchtrainsbuttonId() {
		return searchpageSearchtrainsbuttonId;
	}

	public QAFWebElement getDeparturedateSearchpageXpath() {
		return departuredateSearchpageId;
	}

	@QAFTestStep(description = "user search for the train with details {0} and {1}")
	public static void selectingDetails(String fromCity, String toCity) {

		click("searchpage.fromcitybutton.id");

		QAFExtendedWebElement selectFromCity = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("searchpage.citybutton.xpath"),
				fromCity));
		selectFromCity.click();
		click("searchpage.tocitybutton.id");

		QAFExtendedWebElement selectToCity = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("searchpage.citybutton.xpath"),
				toCity));
		selectToCity.click();

		//click("departuredate.searchpage.id");

		//click("searchpage.departuredate.xpath");

		click("searchpage.searchtrainsbutton.id");

	}

}
