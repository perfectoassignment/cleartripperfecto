package com.cleartrip.steps;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import static com.qmetry.qaf.automation.step.CommonStep.click;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeCatagoryClearTripTestPage
		extends
			WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "cleartrip.homepage.trainscatagorybutton.xpath")
	private static QAFWebElement cleartripHomepageTrainscatagorybuttonXpath;

	@FindBy(locator = "cleartrip.catagorytrainssearchbutton.xpath")
	private QAFWebElement cleartripCatagorytrainssearchbuttonXpath;

	@FindBy(locator = "cleartrip.flights.fromcity")
	private QAFWebElement cleartripFlightsFromcity;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public static QAFWebElement getCleartripHomepageTrainscatagorybuttonXpath() {
		return cleartripHomepageTrainscatagorybuttonXpath;
	}

	public QAFWebElement getCleartripCatagorytrainssearchbuttonXpath() {
		return cleartripCatagorytrainssearchbuttonXpath;
	}

	@QAFTestStep(description = "user selects catagory and search for trains")
	public static void selectCatagory() {
		click("cleartrip.homepage.trainscatagorybutton.xpath");
		click("cleartrip.catagorytrainssearchbutton.xpath");
	}

	public QAFWebElement getCleartripFlightsFromcity() {
		return cleartripFlightsFromcity;
	}

}
