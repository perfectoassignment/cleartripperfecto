package com.cleartrip.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.ui.WebDriverWait;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;


public class LaunchClearTripTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	WebDriverWait wait;
	@FindBy(locator = "cleartrip.launchpagecountrylist.xpath")
	private QAFWebElement cleartripLaunchpagecountrylistXpath;

	@FindBy(locator = "cleartrip.okbuttonlaunchpage.xpath")
	private QAFWebElement cleartripOkbuttonlaunchpageXpath;
	@FindBy(locator = "cleartrip.sliderbuttonlaunchpage.xpath")
	private QAFWebElement cleartripSliderbuttonlaunchpageXpath;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getCleartripLaunchpagecountrylistXpath() {
		return cleartripLaunchpagecountrylistXpath;
	}

	public QAFWebElement getCleartripOkbuttonlaunchpageXpath() {
		return cleartripOkbuttonlaunchpageXpath;
	}

	public QAFWebElement getCleartripSliderbuttonlaunchpageXpath() {
		return cleartripSliderbuttonlaunchpageXpath;
	}

	@QAFTestStep(description = "I select country from launch page")
	public static void selectOKButton() {

		click("cleartrip.okbuttonlaunchpage.xpath");
		click("cleartrip.okbuttonlaunchpage.xpath");
		click("cleartrip.sliderbuttonlaunchpage.xpath");

	}
	public void installApp(String path, String instrumentation) {
		String instrument = "";
		if (instrumentation.equals("true"))
			instrument = "instrument";
		else if (instrumentation.equals("false")) {

		}
		instrument = "noinstrument";

		Map<String, Object> params = new HashMap<>();

		params.put("file", path);
		params.put("instrument", instrument);
		driver.executeScript("mobile:application:install", params);
	}

}
