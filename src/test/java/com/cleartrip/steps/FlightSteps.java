package com.cleartrip.steps;

import org.hamcrest.Matchers;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class FlightSteps extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator = "Tabs.button.homepage")
	private QAFWebElement Tabsbuttonhomepage;
	
	@FindBy(locator = "text.sourcecity.flightPage")
	private QAFWebElement textsourcecityflightPage;
	
	@FindBy(locator = "text.destinationcity.flightPage")
	private QAFWebElement textdestinationcityflightPage;
	
	@FindBy(locator = "textbox.cityEnter.flightPage")
	private QAFWebElement textboxcityEnterflightPage;
	
	@FindBy(locator = "text.suggestedCity.flightPage")
	private QAFWebElement textsuggestedCityflightPage;
	
	@FindBy(locator = "text.travelDatebtn.flightPage")
	private QAFWebElement texttravelDatebtnflightPage;
	
	@FindBy(locator = "label.dateText.flightPage")
	private QAFWebElement labeldateTextflightPage;
	
	@FindBy(locator = "button.classType.flightPage")
	private QAFWebElement buttonclassTypeflightPage;
	
	@FindBy(locator = "radiobutton.classType.flightpage")
	private QAFWebElement radiobuttonclassTypeflightpage;
	
	@FindBy(locator = "button.search.flightPage")
	private QAFWebElement buttonsearchflightPage;
	
	@FindBy(locator = "text.flightList.flightListPage")
	private QAFWebElement textflightListflightListPage;
	
	@FindBy(locator = "text.denyOption.homepage")
	private QAFWebElement textdenyOptionhomepage;
	
	@FindBy(locator = "button.travelOption.homepage")
	private QAFWebElement buttontravelOptionhomepage;
	
	@FindBy(locator = "label.flight.flightpage")
	private QAFWebElement labelflightflightpage;
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user launch the application")
	public void userLaunchTheApplication() {
		System.out.println("Application is launched");
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user selects {0} tab")
	public void userSelectsTab(String tab) {
		QAFExtendedWebElement mainTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("button.navigateTab.homepage"), tab));
		mainTab.click();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user should be navigated to {0} page")
	public void userShouldBeNavigatedToPage(String str0) {
		Validator.verifyThat(labelflightflightpage.isPresent(),
				Matchers.equalTo(true));
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user enter {0} and {1}")
	public void userEnterAnd(String sourceCity, String destinationCity) {
		textsourcecityflightPage.click(); //Click on source btn
		textboxcityEnterflightPage.sendKeys(sourceCity); //enter source
		
		QAFExtendedWebElement suggestedSourceOption = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("text.suggestedCity.flightPage"), sourceCity));
		suggestedSourceOption.click();
		
		textdestinationcityflightPage.click(); //click on destination btn
		textboxcityEnterflightPage.sendKeys(destinationCity); //enter source
		
		QAFExtendedWebElement suggestedDestinationOption = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("text.suggestedCity.flightPage"), destinationCity));
		suggestedDestinationOption.click();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user selects departure date")
	public void userSelectsDepartureDate() {
		texttravelDatebtnflightPage.click();
		//btnnextMonthflightPage.click();
		labeldateTextflightPage.click();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user selects {0}")
	public void userSelects(String classType) {
		buttonclassTypeflightPage.click();
		QAFExtendedWebElement classTypeElement = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("radiobutton.classType.flightpage"), classType));
		classTypeElement.click();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user clicks on search button")
	public void userClicksOnSearchButton() {
		buttonsearchflightPage.click();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user is able to navigate flight listing page")
	public void userIsAbleToNavigateFlightListingPage() {
		Validator.verifyThat(textflightListflightListPage.isPresent(),
				Matchers.equalTo(true));
	}


}
