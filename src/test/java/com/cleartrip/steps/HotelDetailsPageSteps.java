package com.cleartrip.steps;

import org.hamcrest.Matchers;

import com.cleartrip.bean.HotelBean;
import com.cleartrip.pages.HotelDetailsTestPage;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class HotelDetailsPageSteps {

	HotelDetailsTestPage hotelDetailPage = new HotelDetailsTestPage();

	@QAFTestStep(description = "user verify hotel details from details page")
	public void userVerifyHotelDetailsFromDetailsPage() {
		HotelBean bean =
				(HotelBean) ConfigurationManager.getBundle().getProperty("hotel.bean");
		Validator.verifyThat(hotelDetailPage.isHotelNameLablePresent(bean.getHotelName()),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user closes the application")
	public void userClosesTheApplication() {
		try {
			hotelDetailPage.closeApp();
		} catch (Exception e) {
			Reporter.log("application not in opened state");
		}
	}


}
