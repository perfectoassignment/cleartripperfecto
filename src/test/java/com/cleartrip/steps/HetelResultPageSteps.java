package com.cleartrip.steps;

import com.cleartrip.pages.HomeTestPage;
import com.cleartrip.pages.HotelSearchResultsTestPage;
import com.cleartrip.pages.HotelsSearchTestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;


public class HetelResultPageSteps {

	HomeTestPage homePge = new HomeTestPage();
	HotelsSearchTestPage hotelSearchPage = new HotelsSearchTestPage();
	HotelSearchResultsTestPage hotelResultPage = new HotelSearchResultsTestPage();
	
	@QAFTestStep(description = "user selects hotel from results")
	public void userSelectsHotelFromResults() {
		//hotelResultPage.isOkayLnkPresentClick();
		/*try {
			hotelResultPage.applyFilters();
			hotelResultPage.selectHotelByIndex(0);
		} catch (Exception e) {
			hotelResultPage.isOkayLnkPresentClick();
			hotelResultPage.applyFilters();
			hotelResultPage.selectHotelByIndex(0);
		}*/
		hotelResultPage.applyFilters();
		hotelResultPage.selectHotelByIndex(0);
	}

}
